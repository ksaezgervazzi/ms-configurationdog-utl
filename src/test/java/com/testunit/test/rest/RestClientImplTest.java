package com.testunit.test.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.test.client.RestClientImpl;
import com.test.dto.BreedDto;
import com.test.dto.BreedImagesDto;

@RunWith(SpringJUnit4ClassRunner.class)
public class RestClientImplTest {

	@InjectMocks
	RestClientImpl restClientImpl;

	@Mock
	private RestTemplate restTemplate;

	@Before
	public void before() throws Exception {

		ReflectionTestUtils.setField(this.restClientImpl, "apiDogUrlListAllBreeds", "apiDogUrlListAllBreeds");
		ReflectionTestUtils.setField(this.restClientImpl, "apiDogUrlListByBreed", "apiDogUrlListByBreed");

	}

	@SuppressWarnings("unchecked")
	@Test
	public void breedListTestNotNull() {

		String breed = "breedOk";

		List<String> subBreedList = new ArrayList<>();
		subBreedList.add("subBreedOk");

		Map<String, List<String>> messageListAll = new HashMap<>();
		messageListAll.put(breed, subBreedList);
		BreedDto breedDto = BreedDto.builder().message(messageListAll).build();

		Mockito.doReturn(ResponseEntity.ok(breedDto)).when(restTemplate).exchange(Mockito.anyString(),
				Mockito.any(HttpMethod.class), Mockito.any(), Mockito.any(Class.class));

		Assert.assertNotNull(restClientImpl.breedsList());

	}

	@SuppressWarnings("unchecked")
	@Test
	public void breedListTestResponseEntityNull() {

		Mockito.when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.any(),
				Mockito.any(ParameterizedTypeReference.class))).thenThrow(HttpClientErrorException.class);

		Assert.assertNotNull(restClientImpl.breedsList());

	}

	@SuppressWarnings("unchecked")
	@Test
	public void listByBreedTestNotNull() {

		String breed = "breedOk";

		List<String> messageListByBreed = new ArrayList<>();
		messageListByBreed.add("http:imagesBreedOk");
		BreedImagesDto breedImagesDto = BreedImagesDto.builder().message(messageListByBreed).build();

		Mockito.doReturn(ResponseEntity.ok(breedImagesDto)).when(restTemplate).exchange(Mockito.anyString(),
				Mockito.any(HttpMethod.class), Mockito.any(), Mockito.any(Class.class));

		Assert.assertNotNull(restClientImpl.listByBreed(breed));

	}

	@SuppressWarnings("unchecked")
	@Test
	public void blistByBreedTestResponseEntityNull() {

		String breed = "breedOk";

		Mockito.when(restTemplate.exchange(Mockito.anyString(), Mockito.any(HttpMethod.class), Mockito.any(),
				Mockito.any(ParameterizedTypeReference.class))).thenThrow(HttpClientErrorException.class);

		Assert.assertNotNull(restClientImpl.listByBreed(breed));

	}

}
