package com.test.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.test.client.RestClient;
import com.test.dto.BreedDto;
import com.test.dto.BreedImagesDto;

@RestController
public class Controller {

	@Autowired
	RestClient restClient;

	/**
	 * @param
	 * @return BreedDto
	 * @author Karol S�ez.
	 */

	@GetMapping(value = "/breeds", produces = MediaType.APPLICATION_JSON_VALUE)
	public BreedDto breeds() {
		return restClient.breedsList();
	}

	/**
	 * @param breed
	 * @return BreedImagesDto
	 * @author Karol S�ez.
	 */

	@GetMapping(value = "/breed/{breed}/images", produces = MediaType.APPLICATION_JSON_VALUE)
	public BreedImagesDto listByBreed(@PathVariable("breed") String breed) {
		return restClient.listByBreed(breed);
	}

}